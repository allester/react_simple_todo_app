import React from 'react'

import TodoItem from './components/TodoItem'
import todosData from './todosData'

import './App.css'

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      todos: todosData
    }

    this.handleChange = this.handleChange.bind(this)
  }

  // Event handlers
  handleChange(id) {
    this.setState((prevState) => {
      // Identify the item by its ID and then change status
      const updatedTodos = prevState.todos.map((todo) => {
        if (todo.id === id) {
          todo.completed = !todo.completed
        }
        return todo
      })
      return {
        todos: updatedTodos
      }
    })
  }

  render() {
    const todosComponents = this.state.todos.map((item) => {
      return <TodoItem 
                key={item.id}
                item={item}
                handleChange={this.handleChange}
              />
    })
    return (
      <div className="todo-list">
        {todosComponents}
      </div>
    )
  }
}

export default App